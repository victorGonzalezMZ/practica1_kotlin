fun main() {

    println("Ejercicios Kotlin")
    println("Víctor E. González Martínez")

    println("========= TwoSum =========")

    val numArray: IntArray = intArrayOf(15,2,12,17,11,8,6,12,5)
    val tar: Int = 20

    println("Nums: " + numArray.contentToString())
    println("Target: " + tar)

    val result = twoSum(numArray, tar)

    if(result.isEmpty()){
        println("No hay resultado")
    }
    else{
        println("Resultado: " + result.contentToString())
    }

    println("")

    println("========= Plus One =========")

    val digits: IntArray = intArrayOf(4,5,1,2,8)
    println("Digitos (Antes): " + digits.contentToString())

    val end_result = PlusOne(digits)
    println("Digitos (Despues): " + end_result.contentToString())

}

fun twoSum(nums: IntArray, target: Int): IntArray {

    for (i in 0 until nums.size) {
        for (j in i + 1 until nums.size) {
            if (nums[i] + nums[j] == target) {
                return intArrayOf(nums[i], nums[j])
            }
        }
    }

    return intArrayOf()

}

fun PlusOne(digits: IntArray): IntArray {

    val max = digits.maxOrNull() ?: 0
    val sum = max + 1

    for (i in 0 until digits.size) {
        if(digits[i] == max){
            digits[i] = sum
        }
    }

    return digits

}